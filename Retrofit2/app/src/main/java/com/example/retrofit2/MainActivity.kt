package com.example.retrofit2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.recycle_item.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    val BASE_URL = "https://simplifiedcoding.net/demos/"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit =
            Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
                .build()
        val api = retrofit.create(Api::class.java)
        val call = api.getHeroes()
        call.enqueue(object : Callback<List<Hero>> {
            override fun onFailure(call: Call<List<Hero>>, t: Throwable) {
                Toast.makeText(getApplicationContext(), "no json", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<List<Hero>>, response: Response<List<Hero>>) {
                val heroes = response.body()

                val heroNames = arrayListOf<String>()


                for (hero in heroes!!) {
                    heroNames.add(hero.name)
                }

                val adapter=ArrayAdapter<String>(applicationContext,android.R.layout.simple_list_item_1,heroNames)

                listView.adapter=adapter

            }


        })
    }
}
