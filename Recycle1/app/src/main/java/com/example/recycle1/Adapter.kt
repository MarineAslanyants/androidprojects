package com.example.recycle1

import android.app.ProgressDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import androidx.swiperefreshlayout.widget.CircularProgressDrawable.ProgressDrawableSize

class Adapter(val context: Context,val dataList: List<ListItem>) : RecyclerView.Adapter<Adapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view= LayoutInflater.from(context).inflate(R.layout.list_item,parent,false)

        return Holder(view)
    }

    override fun getItemCount(): Int {
        return dataList.count()
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        var list = dataList[position]

        holder.textHeader?.text=list.getHead()
        holder.textdescription?.text=list.getDesc()

    }

    inner class Holder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        val textHeader=itemView?.findViewById<TextView>(R.id.textHeading)
        val textdescription=itemView?.findViewById<TextView>(R.id.textDescription)



    }

    fun getDAtaFromJson(){

    }
}