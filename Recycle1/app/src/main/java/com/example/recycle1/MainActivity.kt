package com.example.recycle1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {


    lateinit var listItems: ArrayList<ListItem>
    lateinit var adapter: Adapter
    val BASE_URL = "https://simplifiedcoding.net/demos/marvel/"
    val BASE_URI ="https://jsonplaceholder.typicode.com/"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        listItems = arrayListOf()


        val retrofit =
            Retrofit.Builder().baseUrl(BASE_URI).addConverterFactory(GsonConverterFactory.create(GsonBuilder().create())).build()






        adapter = Adapter(this, listItems)
        recycleView.setHasFixedSize(true)
        recycleView.layoutManager = LinearLayoutManager(this)
        recycleView.adapter = adapter
    }


}
